/*
 * @author Иванов М.А., КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.core.metrics;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import main.java.by.dev.darkorion.softwarequality.core.languages.Language;

/*
 * Метод analyzeThe анализирует по методу упрощенная
 * цикломатическая сложность кода, подсчитывая количес-
 * тво управляющих опператоров 
 */
public class Mccabe extends Metric {
	
	public Mccabe() {
		super.name = "McCabe - ";
	}
	
	/*
	 * Создает объект класса String
	 * в котором содержится код программы без комментариев.
	 * Далее в этом коде подсчитывается количество управляющих
	 * опператоров.
	 */
	String analyzeThe(String code, Language lang) {
		Pattern patComments = Pattern.compile(lang.getRegComments(), 
				Pattern.MULTILINE | Pattern.DOTALL);
		Matcher matComments = patComments.matcher(code);
		String codeNoComments = matComments.replaceAll("");	
		Pattern patControl =  Pattern.compile(lang.getRegControl(),
				Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
		Matcher matControl = patControl.matcher(codeNoComments);
		int count = 0;
		while (matControl.find()) {
			count++;
		}
		return Integer.toString(count);
	}
}
