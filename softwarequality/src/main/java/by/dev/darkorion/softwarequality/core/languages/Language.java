/*
 * @author Иванов М.А. КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.core.languages;

/*
 * Супер класс для классов языков программирования
 */
public abstract class Language {
	
	String name = null;
	
	/*
	 * регулярное выражение пустой строки
	 */
	String regNotempty = null;
	
	/*
	 * регулярное выражение комментариев в программе
	 */
	String regComments = null;
	
	/*
	 * регулярное выражение управляющих опператоров
	 */
	String regControl = null;
	
	/*
	 * регулярное выражение методов класса
	 */
	String regMetod = null;
	
	
	public String getName() {
		return name;
	}
	
	public String getRegNotEmpty() {
		return regNotempty;
	}
	
	public String getRegComments() {
		return regComments;
	}
	
	public String getRegControl() {
		return regControl;
	}
	
	public String getRegMetod() {
		return regMetod;
	}
}
