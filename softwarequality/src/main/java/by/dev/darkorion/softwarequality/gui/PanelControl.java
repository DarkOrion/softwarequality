/*
 * @author Иванов М.А., КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

import main.java.by.dev.darkorion.softwarequality.actions.AnalyzeAction;

/*
 * Панель с кнопкой "Анализировать" и тесктом "О программе"
 */
class PanelControl extends JPanel {

	private static final long serialVersionUID = -6961985388586166047L;
	
	private static final String LABELABOUT_TXT = 
			"<html>Программный код анализируется по таким метрикам:<br>"
			+ "- CSLOC (количество строк кода)<br>"
			+ "- McCabe metric (цикломатическая сложность кода)<br>"
			+ "- WMPC (сложность класса)<br>"
			+ "<br>"
			+ "Поддержиавемые языки программирования:<br>"
			+ "- Visual Basic (расширение файла .vb)<br>"
			+ "- C++ (расширение файла .cpp)<br>"
			+ "- Java (расширение файла .java)<br>"
			+ "- Mathlab (расширение файла .mat)</html>";
	
	private static final String BUTTONANALIZE_TXT = "Анализировать";
	
	private JPanel panelAbout;
	private JLabel labelAbout;
	
	private JButton buttonAnalyze;
	private Action analyzeAction;
	
	PanelControl(JTextArea codeTextArea, JTextArea resultTextArea,
			JTextField textFieldBrowse) {
		Border border = BorderFactory.createEtchedBorder();   
		GridBagLayout gbl = new GridBagLayout();
		this.setLayout(gbl);
		
		// label о программе
		panelAbout = new JPanel();
		GuiUtil.customizeGbl(gbl, panelAbout, 0, 0, 2, 1, 1.0, 0.02,
				GridBagConstraints.BOTH, 2);
		this.add(panelAbout);		
		labelAbout = new JLabel(LABELABOUT_TXT);
		panelAbout.setBorder(border);
		panelAbout.add(labelAbout);
        
		// button аннализировать код
        analyzeAction = new AnalyzeAction(codeTextArea, 
        		resultTextArea, textFieldBrowse);
		buttonAnalyze = new JButton(analyzeAction);
		buttonAnalyze.setText(BUTTONANALIZE_TXT);
		GuiUtil.customizeGbl(gbl, buttonAnalyze, 1, 1, 1, 1, 1.0, 0.02,
				GridBagConstraints.BOTH, 2);
		this.add(buttonAnalyze);
	}
}
