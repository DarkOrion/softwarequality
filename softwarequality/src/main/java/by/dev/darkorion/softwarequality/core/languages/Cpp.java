/*
 * @author Иванов М.А. КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.core.languages;

/*
 * Содержит регулярные выражения для языка
 * программирования С++
 */
public class Cpp extends Language {
	
	public Cpp() {
		super.name = "Язык программирования - C++\n";
		
		super.regNotempty = "^(\\s)*(\\S)+";
		super.regComments = "(//.*?$)|(/\\*.*?\\*/)";
		super.regControl = "\\s*((\\b(if|for|while)\\s*?\\()"
				+ "|(\\b(case|return|goto|else)\\s*?)"
				+ "|(\\b(default|continue|break|\\}?\\s*(catch))\\s*)"
				+ "|(\\?.+?:.+?))";
		super.regMetod = "(^\\s*([a-zA-Z_0-9&<>]+\\s+){0,4}?\\S+?\\s*)"
				+ "\\((.*?)\\)\\s*\\{"; 
	}
}
