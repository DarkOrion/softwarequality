/*
 * @author Иванов М.А. КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.actions;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JTextField;

/*
 * Обрабатывает событие click buttonBrowse
 */

public class BrowseAction extends AbstractAction {

	private static final long serialVersionUID = 5969058016395609306L;
	
	private JTextField textFieldBrowse;
	
	public BrowseAction(JTextField textFieldBrowse) {
		
		this.textFieldBrowse = textFieldBrowse;
	}

	/*
	 * Вызывает диалог выбора файла и передает
	 * методу setText объекта textFieldBrowse
	 * полный путь к файлу
	 */
	public void actionPerformed(ActionEvent arg0) {

		JFileChooser fileChooser = new JFileChooser();

		fileChooser.setMultiSelectionEnabled(false);
		if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(null)) {
			final String fileName = fileChooser.getSelectedFile()
					.getAbsolutePath();
			textFieldBrowse.setText(fileName);
			textFieldBrowse.repaint();
		}
	}
}
