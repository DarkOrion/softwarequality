/*
 * @author Иванов М.А., КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.gui;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import main.java.by.dev.darkorion.softwarequality.actions.BrowseAction;

/*
 * Панель где выбирается файл для анализа
 */
public class PanelChooseFile extends JPanel {

	private static final long serialVersionUID = -8443785636453070625L;
	
	private static final String BUTTONBROWSE_TXT = "Обзор";
	private static final Dimension TEXTFIELDBROWSE_SIZE =  new Dimension(400, 18);
	
	private String choseFile = "Выберите файл";
	
	private JButton buttonBrowse;
	private Action browseAction;
	private JTextField textFieldBrowse;

	PanelChooseFile () {
        this.setLayout(new FlowLayout());
        
        textFieldBrowse = new JTextField(choseFile);
        textFieldBrowse.setPreferredSize(TEXTFIELDBROWSE_SIZE);
        this.add(textFieldBrowse);
        
        // выбор файла
        browseAction = new BrowseAction(textFieldBrowse);
        buttonBrowse = new JButton(browseAction);
        buttonBrowse.setText(BUTTONBROWSE_TXT);
        this.add(buttonBrowse);     
	}
	
	JTextField getTextFieldBrowse() {		
		return textFieldBrowse;
	}
}
