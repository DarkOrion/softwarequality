/*
 * @author Иванов М.А. КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.core.languages;

/*
 * Содержит регулярные выражения для языка
 * программирования Matlab
 */
public class Mat extends Language {
	
	public Mat() {
		super.name = "Язык программирования - Matlab\n";
		
		super.regNotempty = "^(\\s)*(\\S)+";
		super.regComments = "(%.*?$)";
		super.regControl = ".*?(\\b(if|for|elseif|case|"
				+ "otherwise|return|else|continue|break)\\s+?)";
		super.regMetod = "^(\\s*(function)\\s+\\S+?\\s*)\\((.*?)\\)";
	}
}
