/*
 * @author Иванов М.А. КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.core.languages;

/*
 * Содержит регулярные выражения для языка
 * программирования Java
 */
public class Java extends Language {
	
	public Java() {
		super.name = "Язык программирования - Java\n";
		
		super.regNotempty = "^(\\s)*(\\S)+";
		super.regComments = "(//.*?$)|(/\\*.*?\\*/)";
		super.regControl = "\\s*((\\b(if|for|while)\\s*?\\()"
				+ "|(\\b(case|return|goto|else)\\s*?)"
				+ "|(\\b(default|continue|break|\\}?\\s*(catch))\\s*)"
				+ "|(\\?.+?:.+?))";
		super.regMetod = "(^\\s*([a-zA-Z_0-9&<>]+\\s+){0,4}?\\S+?\\s*)"
				+ "\\((.*?)\\)\\s*\\{"; 
	}
}
