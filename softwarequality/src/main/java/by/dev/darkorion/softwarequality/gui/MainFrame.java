/*
 * @author Иванов М.А., КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;

/*
 * Окно программы
 */
public class MainFrame extends JFrame {

	private static final long serialVersionUID = 2065028368179283174L;

	private static final int DEFAULT_WIDTH = 800;
	private static final int DEFAULT_HEIGHT = 550;
	private static final String DEFAULT_CAPTION = "Software Quality";
	
	private PanelTitle panelTitle;
	private PanelChooseFile panelChooseFile;
	private PanelControl panelControl;
	private PanelCode panelCode;
	private PanelResult panelResult;

	private MainFrame() {
		this.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
		this.setTitle(DEFAULT_CAPTION);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		GridBagLayout gbl = new GridBagLayout();
		this.setLayout(gbl);	

		// панель с названием программы
		panelTitle = new PanelTitle();
		GuiUtil.customizeGbl(gbl, panelTitle, 0, 0, 2, 1, 1.0, 0.02,
				GridBagConstraints.BOTH, 2);
        this.add(panelTitle);
        
		// панель с выбором файла
     	panelChooseFile = new PanelChooseFile();
     	GuiUtil.customizeGbl(gbl, panelChooseFile, 0, 1, 2, 1, 1.0, 0.02,
     			GridBagConstraints.BOTH, 2);
        this.add(panelChooseFile);
		
        // панель с результатом анализа
		panelResult = new PanelResult();
		GuiUtil.customizeGbl(gbl, panelResult, 1, 2, 1, 2, 0.0, 0.0,
				GridBagConstraints.BOTH, 2);
        this.add(panelResult);
		
        // панель с кодом программы
		panelCode = new PanelCode();
		GuiUtil.customizeGbl(gbl, panelCode, 0, 4, 2, 2, 1.0, 0.8,
				GridBagConstraints.BOTH, 2);
        this.add(panelCode);
        
        // панель с кнопками управления
		panelControl = new PanelControl(panelCode.getCodeTextArea(), 
				panelResult.getResultTextArea(),
				panelChooseFile.getTextFieldBrowse());
		GuiUtil.customizeGbl(gbl, panelControl, 0, 2, 1, 2, 0.0, 0.0,
				GridBagConstraints.BOTH, 2);
        this.add(panelControl);		
	}
	
	/* 
	 * Возвращает объект MainFrame
	 */
	public static MainFrame create() {
		return new MainFrame();
	}
}
