/*
 * @author Иванов М.А., КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality;

import javax.swing.SwingUtilities;

import main.java.by.dev.darkorion.softwarequality.gui.MainFrame;

/* 
 * Создаем фрейм в потоке диспетчеризации событий
 */
public class Main {

	public static void main(String[] args) {
		try {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					MainFrame mf = MainFrame.create();
					mf.setVisible(true);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
