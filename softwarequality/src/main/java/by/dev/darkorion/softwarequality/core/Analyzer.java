/*
 * @author Иванов М.А. КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.core;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import main.java.by.dev.darkorion.softwarequality.core.languages.Language;
import main.java.by.dev.darkorion.softwarequality.core.languages.Unknown;
import main.java.by.dev.darkorion.softwarequality.core.metrics.Csloc;
import main.java.by.dev.darkorion.softwarequality.core.metrics.Mccabe;
import main.java.by.dev.darkorion.softwarequality.core.metrics.Metric;
import main.java.by.dev.darkorion.softwarequality.core.metrics.Wmpc;

/*
 * Анализатор кода. Для создания объекта дан-
 * ного класса нужно вызвать метод createAnalyzer
 * передав в параметрах код программы и полный путь
 * к файлу. Метод createAnalyzer вернет объект
 * класса Analyzer.
 * Язык программирования определяется по расширению
 * файла подключая соответствующий класс из пакета
 * languages. Если такого класса в пакете нет то
 * подключается класс Unknown.
 */

public class Analyzer {

	// регулярное выражение расширение файла
	static final String PAT_EXSTENSION = "(?:.+\\.)((cpp|java|mat|vb)$)";
	
	/*
	 * Путь к пакету в котором находятся классы
	 * расширяющие класс Languages
	 */
	static final String PACKAGE_LANGUAGES = "main.java.by.dev.darkorion"
			+ ".softwarequality.core.languages.";

	private String code;
	private Language language;
	private ArrayList<Metric> metrics;
	private ArrayList<String> result;

	private Analyzer(String code, Language language) {
		this.code = code;
		this.language = language;
		metrics = new ArrayList<Metric>();
		metrics.add(new Csloc());
		metrics.add(new Mccabe());
		metrics.add(new Wmpc());
		result = new ArrayList<String>();
	}
	
	/*
	 * Метод createAnalyzer вернет объект
	 * класса Analyzer.
	 * Язык программирования определяется по расширению
	 * файла подключая соответствующий класс из пакета
	 * languages. Если такого класса в пакете нет то
	 * подключается класс Unknown.
	 */
	public static Analyzer createAnalyzer(String filePath, String code) {
		Pattern patExtension = Pattern.compile(PAT_EXSTENSION);
		Matcher matcher = patExtension.matcher(filePath);
		Language language = new Unknown();
		if (matcher.matches()) {
			String ext = matcher.group(matcher.groupCount() - 1);
			String classLang;
			classLang = ext.substring(0, 1).toUpperCase()
					+ ext.substring(1).toLowerCase();
			Object lang;
			
			// проверяет на ошибки рефлексии
			try {			
				lang = Class.forName(PACKAGE_LANGUAGES + classLang).newInstance();
				language = (Language)lang;		
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return new Analyzer(code, language);
	}
	
	/*
	 * Находит результат анализа кода
	 * по очереди применяя метрики к коду.
	 * для этого вызывается метод ifPossibleAnalyzeThe
	 * объекат класса Metric с параметрами:
	 * String - код программы, 
	 * обьект подкласса Language.
	 */
	public ArrayList<String> findResult() {
		result.add(language.getName());
		for (Metric metric : metrics) {
			result.add(metric.ifPossibleAnalyzeThe(code, language));
		}
		return result;
	}

}

