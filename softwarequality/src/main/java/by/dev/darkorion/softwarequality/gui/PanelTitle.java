/*
 * @author Иванов М.А., КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.gui;

import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JPanel;

/*
 * Панель с названием программы
 */
class PanelTitle extends JPanel {

	private static final long serialVersionUID = -4971619016154424658L;
	
	private static final String TITLE = "ВЫЧИСЛЕНИЕ МЕТРИК РАЗМЕРА И СЛОЖНОСТИ ПРОГРАММ";
    private static final Font FONT_TITLE = new Font("Arial", Font.BOLD, 22);
	
	private JLabel titleLabel;
	
	PanelTitle() {
        titleLabel = new JLabel(TITLE);
        titleLabel.setFont(FONT_TITLE);
        this.add(titleLabel);
	}
}
