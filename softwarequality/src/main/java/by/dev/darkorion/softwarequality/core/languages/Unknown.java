/*
 * @author Иванов М.А. КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.core.languages;

/*
 * Если не удалось определить язык программирования
 * по расширению файла
 */
public class Unknown extends Language {
	
	public Unknown() {
		super.name = "Данный язык программирования не поддерживается\n";
	}
}
