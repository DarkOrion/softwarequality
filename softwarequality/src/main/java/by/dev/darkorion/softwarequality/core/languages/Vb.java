/*
 * @author Иванов М.А. КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.core.languages;

/*
 * Содержит регулярные выражения для языка
 * программирования Visual Basic
 */
public class Vb extends Language {
	
	public Vb() {
		super.name = "Язык программирования - Visual Basic\n";
		
		super.regNotempty = "^(\\s)*(\\S)+";
		super.regComments = "(\\'.*?$)";
		super.regControl = "^\\s*?(\\b(if|for|case|return|"
				+ "goto|else|do|continue|exit|iif)\\s+?)";
		super.regMetod = "^\\s*([a-zA-Z_0-9&]+\\s+){0,3}?((sub|function)"
				+ "\\s+\\S+?\\s*)\\((.*?)\\)";
	}
}
