/*
 * @author Иванов М.А., КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.core.metrics;

import main.java.by.dev.darkorion.softwarequality.core.languages.Language;

/*
 * Супер класс для классов метрик
 */
public abstract class Metric {

	private static final String NOT_ENOUGH_DATA = "не достаточно данных";
	
	private String result;
	String name;

	/*
	 * Вызывает метод analyzeThe определенный в
	 * подклассе. Если генерируется ошибка NullPointerException,
	 * то значит  метод анализа не нашел регулярное выражение
	 * в классе одного из языков программирования. В этом случае
	 * результат анализа -  NOT_ENOUGH_DATA
	 */
	public String ifPossibleAnalyzeThe(String code, Language lang) {
		try {
			result = analyzeThe(code, lang);
		} catch (NullPointerException e) {
			result = NOT_ENOUGH_DATA;
		}
		return name + result;
	}

	abstract String analyzeThe(String code, Language lang);
}
