/*
 * @author Иванов М.А., КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.gui;

import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;

/*
 * Панель с результатом анализа кода
 */
class PanelResult extends JPanel {

	private static final long serialVersionUID = -2677380257750351328L;
	
	private static final String TITLE_BORDER = "Результат";
	
	private JTextArea resultTextArea;
	private JScrollPane scrollPane;

	PanelResult() {
		Border bord = BorderFactory.createEmptyBorder();
        Border border = BorderFactory.createTitledBorder(bord, TITLE_BORDER);
        this.setBorder(border);
        
        this.setLayout(new GridLayout());
        
        resultTextArea = new JTextArea();
        resultTextArea.setEditable(false);
        scrollPane = new JScrollPane(resultTextArea);
        this.add(scrollPane);
	}
	
	JTextArea getResultTextArea() {
		return resultTextArea;
	}
}
