/*
 * @author Иванов М.А., КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.gui;

import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;

/*
 * Панель с кодом программы считаного из файла
 */
class PanelCode extends JPanel {

	private static final long serialVersionUID = 1085846844465124139L;
	
	private static final String TITLE_BORDER = "Код программы";
	
	private JTextArea codeTextArea;
	private JScrollPane scrollPane;

	PanelCode() {
		Border bord = BorderFactory.createEmptyBorder();
        Border border = BorderFactory.createTitledBorder(bord, TITLE_BORDER);
        this.setBorder(border);
        
        this.setLayout(new GridLayout());
        
        codeTextArea = new JTextArea();
        codeTextArea.setEditable(false);
        scrollPane = new JScrollPane(codeTextArea);
        this.add(scrollPane);
	}
	
	JTextArea getCodeTextArea() {
		return codeTextArea;
	}
}
