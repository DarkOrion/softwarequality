/*
 * @author Иванов М.А., КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.core.metrics;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import main.java.by.dev.darkorion.softwarequality.core.languages.Language;

/*
 * Метод analyzeThe подсчитывает количество строк в коде
 * включая комментарии 
 */
public class Csloc extends Metric {
	
	public Csloc() {
		super.name = "CSLOC - ";
	}

	String analyzeThe(String code, Language lang) {
		Pattern patNotEmpty = Pattern.compile(lang.getRegNotEmpty(),
				Pattern.MULTILINE);
		Matcher matNotEmpty = patNotEmpty.matcher(code);
		int count = 0;
		while (matNotEmpty.find()) {
			count++;
		}
		return Integer.toString(count);
	}
}
