/*
 * @author Иванов.М.А., КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;

public class GuiUtil {
	
	// создает пустой Jlabel
	static JLabel createEmptyLabel() {
		JLabel label = new JLabel();
		label.setPreferredSize(new Dimension(100, 30));
		return label;
	}
	
	// настраивает GridBagLayots
	static void customizeGbl(GridBagLayout gbl, Component component, int gridx,
			int gridy, int gridwidth, int gridheight, double weightx,
			double weighty, int fill, int ins) {

		GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = gridx;
		gbc.gridy = gridy;
		gbc.gridwidth = gridwidth;
		gbc.gridheight = gridheight;
		gbc.weightx = weightx;
		gbc.weighty = weighty;
		gbc.fill = fill;
		gbc.insets = new Insets(ins, ins, ins, ins);

		gbl.setConstraints(component, gbc);
	}
}
