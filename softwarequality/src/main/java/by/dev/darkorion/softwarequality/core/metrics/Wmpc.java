/*
 * @author Иванов М.А., КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.core.metrics;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import main.java.by.dev.darkorion.softwarequality.core.languages.Language;

/*
 * Сложность класса WMPC
 */
public class Wmpc extends Metric {
	
	public Wmpc() {
		super.name = "WMPC - ";
	}

	/*
	 * Создает объект класса String
	 * в котором содержится код программы без комментариев.
	 * Далее в этом коде подсчитывается количество методов 
	 * классе.
	 */
	String analyzeThe(String code, Language lang) {
		Pattern patComments = Pattern.compile(lang.getRegComments(), 
				Pattern.MULTILINE | Pattern.DOTALL);
		Matcher matComments = patComments.matcher(code);
		String codeNoComments = matComments.replaceAll("");
		Pattern patMetod = Pattern.compile(lang.getRegMetod(), 
				Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
		Matcher matMetod = patMetod.matcher(codeNoComments);
		int count = 0;
		Pattern patControl;
		Matcher matControl;
		while (matMetod.find()) {
			patControl = Pattern.compile(lang.getRegControl(),
					Pattern.MULTILINE | Pattern.CASE_INSENSITIVE);
			matControl = patControl.matcher(matMetod.group());
			if (!matControl.find()) {
				count++;
			}
		}
		return Integer.toString(count);
	}
}
