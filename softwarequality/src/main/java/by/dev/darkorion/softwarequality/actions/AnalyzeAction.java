/*
 * @author Иванов М.А. КНТз-410
 */
package main.java.by.dev.darkorion.softwarequality.actions;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import main.java.by.dev.darkorion.softwarequality.core.Analyzer;

/*
 * Обрабатывает событие click buttonAnalyze.
*/
public class AnalyzeAction extends AbstractAction {
	
	private static final long serialVersionUID = 8760735774368228231L;
	
	private JTextArea codeTextArea;
	private JTextArea resultTextArea;
	private JTextField textFieldBrowse;
	
	private Analyzer analyzer;
	
	private ArrayList<String> result;
	
	public AnalyzeAction( JTextArea codeTextArea, JTextArea resultTextArea,
    		JTextField textFieldBrowse) {
		this.codeTextArea = codeTextArea;
		this.resultTextArea = resultTextArea;
		this.textFieldBrowse = textFieldBrowse;
	}

	/*
	 * Cтатический метод actionPerformed класса Anlyzer
	 * создает объект Analyzer и передает его методу findResult, 
	 * в параметрах, прочитанный из файла тект программы и путь к 
	 * файлу. Возвращенный результат анализа типа ArraList<String>
	 * передается в методе setText объекта resultTextArea
	 */
	public void actionPerformed(ActionEvent arg0) {
		codeTextArea.setText("");
		resultTextArea.setText("");
        BufferedReader in;
        String s;
        StringBuffer s2;
        try {
            in = new BufferedReader(new FileReader(textFieldBrowse.getText()));
            s2 = new StringBuffer();
            while ((s = in.readLine()) != null) {
                s2.append(s + "\n") ;
            }
            codeTextArea.setText(s2.toString());
            in.close();
            
            analyzer = Analyzer.createAnalyzer(textFieldBrowse.getText(),
            		codeTextArea.getText());
            result = analyzer.findResult();
            for (String res : result) {
            	if (res != null) {
            		resultTextArea.append(res);
            		resultTextArea.append("\n");
            	}		
			}
        } catch (FileNotFoundException fileNotFoundException) {
			resultTextArea.setText("Файл не найден.");
		} catch (IOException ioException) {
			resultTextArea.setText("Ошибка ввода-вывода.");
		} catch (Exception e) {
			resultTextArea.setText("Ошибка в программе:\n" + e.toString());
		}
        
        codeTextArea.repaint();
        resultTextArea.repaint();
	}
}
